from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
from django.contrib.auth.decorators import login_required


@login_required
def show_receipts(request):
    lists= Receipt.objects.filter(purchaser=request.user)
    context= {
        "lists":lists,
    }
    return render(request, "receipts/list.html", context)

def redirect_to_page(request):
    return redirect("home")

@login_required
def create_receipt(request):
    if request.method=="POST":
        form= ReceiptForm(request.POST)
        if form.is_valid():
            receipt=form.save(False)
            receipt.purchaser=request.user
            receipt.save()
            return redirect("home")
    else:
        form=ReceiptForm()
    context={
        "form":form,
    }
    return render(request, "receipts/create.html", context)

@login_required
def show_category(request):
    expenses= ExpenseCategory.objects.filter(owner=request.user)
    context={
        "expenses":expenses,
    }
    return render(request, "receipts/category.html", context)

@login_required
def show_account(request):
    account= Account.objects.filter(owner=request.user)
    context={
        "account":account,
    }
    return render(request, "receipts/accounts.html", context)

@login_required
def create_category(request):
    if request.method=="POST":
        form= CategoryForm(request.POST)
        if form.is_valid():
            receipt=form.save(False)
            receipt.owner=request.user
            receipt.save()
            return redirect("category_list")
    else:
        form=CategoryForm()
    context={
        "form":form,
    }
    return render(request, "receipts/create_category.html", context)

@login_required
def create_account(request):
    if request.method=="POST":
        form= AccountForm(request.POST)
        if form.is_valid():
            receipt=form.save(False)
            receipt.owner=request.user
            receipt.save()
            return redirect("account_list")
    else:
        form=AccountForm()
    context={
        "form":form,
    }
    return render(request, "receipts/create_account.html", context)
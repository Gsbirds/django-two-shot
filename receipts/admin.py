from django.contrib import admin
from .models import Account, Receipt, ExpenseCategory
# Register your models here.
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display=(
        "name",
        "owner",
        "id",
    )
admin.site.register(ExpenseCategory, ExpenseCategoryAdmin)

class AccountAdmin(admin.ModelAdmin):
    list_display=(
        "name",
        "number",
        "owner",
        "id"
    )
admin.site.register(Account, AccountAdmin)

class ReceiptAdmin(admin.ModelAdmin):
    list_display=(
    "vendor",
    "total",
    "tax",
    "date",
    "purchaser",
    "id"
    )
admin.site.register(Receipt, ReceiptAdmin)